FROM fedora:28
MAINTAINER bashton@brennanashton.com

RUN dnf install -y ncurses-compat-libs make git; dnf clean all
# xsdk requires X11 even in batch mode...
RUN dnf install -y libXtst; dnf clean all
